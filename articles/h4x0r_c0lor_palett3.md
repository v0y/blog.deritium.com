# h4x0r_c0lor_palett3

If you're bored with [solarized](https://ethanschoonover.com/solarized/) and switching between light and darth theme does't help anymore - try h4x0r color palette!

It's designed to give you h4x0r feeling, but also to be readable and good looking in various applies.

![](media/h4x0r_c0lor_palett3/palette.png)


name | hex
--- | ---
red | `#F20C04`
yellow | `#FFF105`
cyan | `#FF3DA3`
blue | `#16B1FF`
cyan | `#2AEEEE`
green | `#00FF5E`
white | `#D6D6D6`
light gray | `#BCBCBC`
dark gray | `#808080`
black | `#181818`


## Screenshots

[Source code](https://github.com/tiangolo/fastapi/blob/e1758d107ea5eede358cfdbf69ae7829c8e65a92/fastapi/routing.py#L125):
![]source code(media/h4x0r_c0lor_palett3/py_code.png)

[Rofi](https://github.com/davatorium/rofi):
![rofi](media/h4x0r_c0lor_palett3/drun.png)

[htop](https://htop.dev/):
![htop](media/h4x0r_c0lor_palett3/htop.png)

[Dunst](https://dunst-project.org/):
![dunst](media/h4x0r_c0lor_palett3/dunst.png)
Download Dunst config below!


## Downloads

- [Dunst config](https://gitlab.com/v0y/blog.deritium.com/-/raw/master/articles/media/h4x0r_c0lor_palett3/dunstrc) (`~/.config/dunst/dunstrc`)
- TODO: IntelliJ IDEA color scheme
- TODO: Terminator scheme
- TODO: i3wm + rofi theme
- TODO: ptpython theme
- TODO: moar
