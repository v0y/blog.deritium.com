# blog.deritium.com

## Run

 docker run -d \
 --name blog_deritium_com \
 -p 2368:2368 \
 -v /root/code/blog.deritium.com/data:/var/lib/ghost/content \
 -e url=http://blog.deritium.com \
 ghost